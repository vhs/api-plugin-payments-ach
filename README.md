# api-plugin-payments-ach

[![npm](https://img.shields.io/npm/v/api-plugin-payments-ach?style=flat-square)](https://www.npmjs.com/package/api-plugin-payments-ach)
[![downloads](https://img.shields.io/npm/dm/api-plugin-payments-ach.svg?style=flat-square)](https://www.npmjs.com/package/api-plugin-payments-ach)
[![license](https://img.shields.io/npm/l/api-plugin-payments-ach.svg?style=flat-square&longCache=true)](https://codeberg.org/vhs/api-plugin-payments-ach/src/branch/trunk/COPYING)

## Summary

Contributor ACH Payments plugin for the Reaction API. Designed for [Reaction Commerce](https://reactioncommerce.com) 3.0+.

## Motivation

Stripe [restricts payments](https://stripe.com/restricted-businesses) to some businesses. In order to make Reaction Commerce useful for those businesses and mitigate Stripe as a single point of failure for others, users may accept ACH payments directly to US banking institutions. This plug-in helps facilitate those payments.

## Tutorial

[How to accept ACH payments using Reaction Commerce.](https://vhs.codeberg.page/post/ach-payments-reaction-commerce/)

## Screenshots

![](docs/order-details-fs8.png)

## License
This Reaction plugin is [GNU GPLv3 Licensed](./LICENSE.md)
