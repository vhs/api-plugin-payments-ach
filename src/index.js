import pkg from "../package.json";
import i18n from "./i18n/index.js";
import schemas from "./schemas/index.js";
import achCapturePayment from "./util/achCapturePayment.js";
import achCreateAuthorizedPayment from "./util/achCreateAuthorizedPayment.js";
import achCreateRefund from "./util/achCreateRefund.js";
import achListRefunds from "./util/achListRefunds.js";
import startup from "./startup.js";

/**
 * @summary Import and call this function to add this plugin to your API.
 * @param {ReactionAPI} app The ReactionAPI instance
 * @returns {undefined}
 */
export default async function register(app) {
  await app.registerPlugin({
    label: "ACH Transfers",
    name: "payments-ach",
    version: pkg.version,
    i18n,
    graphQL: {
      schemas
    },
    functionsByType: {
      startup: [startup]
    },
    paymentMethods: [{
      name: "ach_transfer",
      canRefund: true,
      displayName: "ACH Transfer",
      functions: {
        capturePayment: achCapturePayment,
        createAuthorizedPayment: achCreateAuthorizedPayment,
        createRefund: achCreateRefund,
        listRefunds: achListRefunds
      }
    }]
  });
}
