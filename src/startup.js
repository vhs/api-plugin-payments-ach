/**
 * @summary Called on startup
 * @param {Object} context Startup context
 * @param {Object} context.collections Map of MongoDB collections
 * @returns {undefined}
 */
export default function achPaymentsStartup(context) {
  context.collections.ACHPaymentRefunds = context.app.db.collection("ACHPaymentRefunds");
}
